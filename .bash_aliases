# .bash_aliases at krai shark
# v 1.1 | 08-15
#
alias ll='ls -alhF'
alias la='ls -A'
alias l='ls -CF'
alias df='df -H'
alias du='du -ch'
alias cd..='cd ..'
alias ping='ping -c 5'
alias wget='wget -c'
alias psinfo="ps -e -o pid,args | grep"
alias pss="ps aux | grep "
alias rmate="${HOME}/gems/bin/rmate -p 52814"
alias flowrstatus="flowr status x=/rsrch2/genomic_med/krai/flowr/runs/"

# extract archives
extract () {
     if [ -f $1 ] ; then
         case $1 in
             *.tar.bz2)   tar xjf $1        ;;
             *.tar.gz)    tar xzf $1     ;;
             *.bz2)       bunzip2 $1       ;;
             *.rar)       rar x $1     ;;
             *.gz)        gunzip $1     ;;
             *.tar)       tar xf $1        ;;
             *.tbz2)      tar xjf $1      ;;
             *.tgz)       tar xzf $1       ;;
             *.zip)       unzip $1     ;;
             *.Z)         uncompress $1  ;;
             *.7z)        7z x $1    ;;
             *)           echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}

###

# Level Up by # (min 1) using up <3> command:
up(){
  local d=""
  limit=$1
  for ((i=1 ; i <= limit ; i++))
    do
      d=$d/..
    done
  d=$(echo $d | sed 's/^\///')
  if [ -z "$d" ]; then
    d=..
  fi
  cd $d
}

###

# cd to directory and list contents
cdl()    {
  cd "$@";
  ls;
}
###

#make and move to new dir
mkcd(){
mkdir -p "$*"
cd "$*"
}

deephs(){
if [ "$#" -lt 1 ]; then
  echo "Usage: deephs query or deephs "bedtools.*wgEncode.*bed""
else
  grep -rhn "$1" ~/.history/
fi
}

###

