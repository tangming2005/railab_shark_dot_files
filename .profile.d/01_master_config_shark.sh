#!/bin/bash

## shark hpc .profile.d/01_master_config.sh
## v 2.1 | 08/2016
## @sbamin

## NOTE ##
# config is nearly identical to that of HPC Nautilus EXCEPT a few lines
# related to perl library setup and USER defined PATH variables
# Use diff command to compare this file against HPC Nautilus branch instead of
# overwriting it with changes made into nautilus side of this file
##########

################ 01. Start exporting PATH ################
export GMAPPS="/scratch/genomic_med/apps";
export GMLIBS="/scratch/genomic_med/libs";
export MYRSRCH1="/rsrch1/genomic_med/gm_dep/${USER}"
export PERL5LIB=$PERL5LIB:/scratch/genomic_med/apps/ensembl-vep

## HOME specific paths ##
mypathmunge ${HOME}/bin

# mypathmunge ${HOME}/.linuxbrew/bin
# mypathmunge ${HOME}/lib/lib64/openssl/bin
# mypathmunge ${HOME}/lib/lib64/curl/default/bin

mypathmunge ${HOME}/.aspera/connect/bin after

## GMAPPS paths ##
mypathmunge /scratch/genomic_med/bin after
mypathmunge $GMAPPS/ncdu/default/bin after
mypathmunge $GMAPPS/ngsplot/default/bin after
mypathmunge $GMAPPS/kentutils/default after
mypathmunge $GMAPPS/genetorrent/default/bin after
mypathmunge $GMAPPS/weblogo/default after
mypathmunge $GMAPPS/ghostscript/default/bin after
mypathmunge $GMAPPS/homer/default/bin after
mypathmunge $GMAPPS/rose/default after
mypathmunge $GMAPPS/bedtools/default after
mypathmunge $GMAPPS/subread/default/bin after
mypathmunge $GMAPPS/subread/default/bin/utilities after
mypathmunge $GMAPPS/kallisto/default/bin after
mypathmunge $GMAPPS/bedops/default after
mypathmunge $GMAPPS/sratools/sratoolkit.2.8.2-1-centos_linux64/bin after
mypathmunge $GMAPPS/RSEM after
mypathmunge $GMAPPS/ataqv/usr/bin after
mypathmunge $GMAPPS/freebayes/freebayes/bin after
mypathmunge $GMAPPS/freebayes/freebayes/scripts after
mypathmunge $GMAPPS/freebayes/freebayes/vcflib/bin
mypathmunge $GMAPPS/vt after
mypathmunge $GMAPPS/ensembl-vep after
mypathmunge $GMAPPS/ensembl-vep/htslib before
mypathmunge $GMAPPS/vcfanno
#mypathmunge $GMAPPS/bwakit/bwa.kit after
#mypathmunge $GMAPPS/ngsCAT/ngscat.v0.1
mypathmunge $GMAPPS/lancet
mypathmunge $GMAPSS/bigly
mypathmunge $GMAPPS/bam-readcount/build/bin
export NCM_HOME=$GMAPPS/ngscheckmate/NGSCheckMate after
mypathmunge $GMAPPS/methyldackel/MethylDackel_build
mypathmunge $GMAPPS/trim_galore
mypathmunge $GMAPPS/gistic after 
mypathmunge $GMAPPS/bwa/bwa-0.7.17 after
## use user perl modules
module load module load perl/5.22.1
eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`
export MANPATH=$HOME/perl5/man:$MANPATH

## pigz: parallel gzip - make sure to do: module load zlib/1.2.8 (v1.2.8 or more)
# mypathmunge $GMAPPS/pigz/default after
## mpibzip2: parallel and MPI optimized fast compression/decompression - load modules: zlib and openmpi.
# mypathmunge $GMAPPS/mpibzip2/default after
# mypathmunge /risapps/rhel5/perl/5.18.1/bin
# mypathmunge $GMAPPS/paradigm_shift/paradigmshift/bin after
# mypathmunge $GMAPPS/bpipe/default/bin after
# mypathmunge $GMAPPS/samtools/latest/bin after
# mypathmunge $GMAPPS/bcftools/latest/bin after

# mypathmunge $GMAPPS/cufflinks/default after
# mypathmunge $GMAPPS/sailfish/default/bin after
# mypathmunge $GMAPPS/gfold/default after
# mypathmunge $GMAPPS/exonerate/default/bin after
# mypathmunge $GMAPPS/tophat2/default after
# mypathmunge $GMAPPS/phred after
# mypathmunge $GMAPPS/phrap after
# mypathmunge $GMAPPS/smuffin/bin after
# mypathmunge $GMAPPS/hisat/beta after
# mypathmunge $GMAPPS/RepeatMasker/RepeatMasker after
# mypathmunge $GMAPPS/ngsutils/ngsutils/bin after

## ruby specific:
mypathmunge $GMAPPS/ruby/default/bin
export GEM_HOME=$HOME/gems
export RUBYOPT=rubygems
mypathmunge $HOME/gems/bin after

################### 01. End exporting PATH ##################

################ 02. Start exporting modules ################
# module load gcc/4.8.1 # load gcc before LD_PATH under 03_hpclibs.sh - to fix Rsamtools in R.
module load cmake/3.4.1
module load autoconfig/2.69
module load boost/1.57.0
module load git/2.3.5
# module load ImageMagick/6.9.0-0
# module load mpich3/3.1.3-shark

# module load bedtools/2.24.0
module load bowtie2/2.2.3
#module load bwa/0.7.10
module load samtools/1.2
module load cghub/3.8.6-130
module load fastqc/0.10.1
module load jdk/1.8.0_45
module load libpng/1.6.16
# module load gatk/3.1-1
# module load macs2/2.1.0
# module load picard/1.67-risapps
# module load star/2.4.0j

## zlib and openmpi for fast decompression of tar.gz and bzip2 archives.
module load zlib/1.2.8
# module load openmpi/1.8.4

## User perlbrew instead of module load
module load perl/5.10.1
## use R/3.4.1 for a more recent tidyverse package
#module load R/3.4.1-shlib

############################ 02. End exporting modules #########################

################### 03. Start exporting libs and final config ##################
######################################## STABLE ENV ########################################

#### SETUP Anaconda Python and R ####
## 1. prefix anaconda python to your PATH
	# mypathmunge ${HOME}/anaconda/bin

	# alternately use GMAPPS anaconda
    mypathmunge ${GMAPPS}/python/anaconda/default/bin

## 2. Load default R 3.2.3: with shared lib for running rpy2 ipython env
# load R 3.2.3 after loading anaconda python to override R packaged with anaconda (v3.3.1)
mypathmunge /scratch/genomic_med/apps/R/R4py2/bin
#mypathmunge /risapps/rhel6/R/3.4.1-shlib/bin/R
# one-time execute following command where your own R packages will be installed:
# mkdir -p ~/R/R-3.2.3/lib64/R/library
# Also, in your ~/.Renviron file, add following line to allow R to access GMAPPS R packages
# R_LIBS="~/R/R-3.2.3/lib64/R/library:/scratch/genomic_med/apps/R/R-3.2.3-shlib/lib64/R/library:/scratch/genomic_med/apps/R/R-3.2.3/lib64/R/library"
#### END Anaconda Python and R SETUP ####

## load LD PATHS and binary path for custom libraries after loading R and anaconda but before custom speedseq at gmapps. Do not load speedseq from RIS modules.

## Following order for setting up LD PATHS and PATH is important else some of program would not work
# e.g., module list or error related to missing correct version for GLIBCXX
# load system libs first, followed by updated GCC (to fix GLIBCXX error), and finally user defined custom libs (HDF5, readline, etc.) and binaries
export LD_LIBRARY_PATH=/lib64:/usr/lib64:${LD_LIBRARY_PATH}
module unload gcc
module load gcc/4.8.5
mypathmunge /scratch/genomic_med/libs/lib64/bin after
export LD_LIBRARY_PATH=/scratch/genomic_med/libs/lib64/lib:/risapps/rhel6/mpich-3.1.3/lib:${LD_LIBRARY_PATH}
mypathmunge /risapps/rhel6/mpich-3.1.3/bin after
############################

##### Custom Speedseq #####
#export PATH=/scratch/genomic_med/apps/spseq/speedseq/bin:${PATH}
module load root/5.34_25
#####

#### freebayes parallel ###

export  LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/scratch/genomic_med/apps/freebayes/freebayes/SeqLib/htslib
####

##### CHIPSEQ PIPELINE #####
## MACS and MACS2 are now installed using anaconda. Following paths for binaries and PYTHONPATH will not work.
#mypathmunge $GMAPPS/macs/macs1/bin after
#mypathmunge $GMAPPS/macs/macs2/bin after
#export PYTHONPATH=/scratch/genomic_med/apps/macs/macs1/lib/python2.7/site-packages:/scratch/genomic_med/apps/macs/macs2/lib/python2.7/site-packages"${PYTHONPATH:+:$PYTHONPATH}"

export NGSPLOT=${GMAPPS}/ngsplot/default
#####

#### STAR RNAseq ####
mypathmunge ${GMAPPS}/star/default/bin after
mypathmunge ${GMAPPS}/star/default/STAR-Fusion after
export PERL5LIB=${GMLIBS}/perl/perl5/lib/site_perl"${PERL5LIB:+:$PERL5LIB}"

## activate perlbrew
source /scratch/genomic_med/libs/perl/perlbrew/etc/bashrc
####

#### blasr PacBio ####

# export LD_LIBRARY_PATH=${GMAPPS}/python/anaconda/default/lib/:${GMAPPS}/blasr/default/blasr/libcpp/alignment:${GMAPPS}/blasr/default/blasr/libcpp/hdf:${GMAPPS}/blasr/default/blasr/libcpp/pbdata:${GMAPPS}/blasr/default/hdf5/hdf5-1.8.16-linux-centos6-x86_64-gcc447-shared/lib/:${LD_LIBRARY_PATH}

# mypathmunge ${GMAPPS}/blasr/default/blasr after

####

##### bam-matcher #####
# mypathmunge $GMAPPS/bammatcher/default after
## Following steps are only required if you do not know GATK and JAVA explicit paths ##
# gatk will load jdk 1.8, may conflict with default jdk 1.6: unload latter.
# use path of gatk in bam-matcher.conf from module show gatk/3.5-0
# module load gatk/3.5-0
# module unload jdk/1.6.0_23
####

##### Build specific env #####
# CXXFLAGS="-I/usr/mpi/gcc/openmpi-1.8.4/include"
# CPPFLAGS="-I/usr/mpi/gcc/openmpi-1.8.4/include"
# LDFLAGS="-L/usr/mpi/gcc/openmpi-1.8.4/lib64"
#
## Force cmake to use updated gcc compiler:
# http://stackoverflow.com/a/24380618/1243763
# CC=/risapps/rhel6/gcc/4.8.5/bin/gcc
# CXX=/risapps/rhel6/gcc/4.8.5/bin/c++
## use -fPIC flag for compiling R using shlib option for rpy2 python package
#CC="gcc -fPIC"
#CXX="g++ -fPIC"
#####

#### remove unwanted module ####
module unload jdk/1.6.0_23

################################## END STABLE ################################

############################## START EXPERIMENTAL ############################

#### OpenSSL - ERRORED ####
#mypathmunge ${GMLIBS}/openssl/bin
#export LD_LIBRARY_PATH=${GMLIBS}/openssl/lib:${LD_LIBRARY_PATH}
#export CXXFLAGS="-g -O3 -I$GMLIBS/gsl/default/include -L$GMLIBS/gsl/default/lib";


#export LD_LIBRARY_PATH="$GMLIBS/boost/lib:$GMLIBS/gsl/default/lib:$GMAPPS/sailfish/default/lib":$LD_LIBRARY_PATH;
#export PHRED_PARAMETER_FILE="$GMAPPS/phred/PhredPar/phredpar.dat";
# export BROADHG19="/scratch/rists/hpcapps/reference/human/broad_hg19";
# export PYTHONPATH=/scratch/genomic_med/lib/python:/scratch/genomic_med/lib/python2.7"${PYTHONPATH:+:$PYTHONPATH}"
#export PERL5LIB=/risapps/rhel5/perl/5.18.1/lib/site_perl/5.18.1:${GMLIBS}/perl/perl_v5.10.1"${PERL5LIB:+:$PERL5LIB}"
#export LD_LIBRARY_PATH=${GMLIBS}/lib64:/usr/lib64"${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
# export NGSPLOT=${GMAPPS}/ngsplot/default

### For R and python to work as well as Rsamtools libstdc++.so.6 issue, load gcc module followed by exporting gcc libs in LD path.
#### UNSTABLE  - TESTING speedseq and R runs ####
# module load R/3.1.0-risapps

## module load speedseq/0.1.0
## export LD_LIBRARY_PATH=/usr/lib64:${LD_LIBRARY_PATH}
## module unload python/2.7.6-speedseq
## export PATH=/scratch/genomic_med/apps/python/v2.7/bin:${GMLIBS}/bin:$PATH
#export PATH=/scratch/genomic_med/gm_dep/samin1/anaconda/bin:${PATH}
#export PATH=/scratch/genomic_med/apps/python/anaconda/default/bin:${PATH}
## export PYTHONPATH=${GMLIBS}/lib/python2.7/site-packages"${PYTHONPATH:+:$PYTHONPATH}"
## export LD_LIBRARY_PATH=${GMLIBS}/lib64/symlinks:${LD_LIBRARY_PATH}
#### END TEST ####

#module load gcc/4.8.1
## export LD_LIBRARY_PATH="${GMLIBS}/lib64/libreadline-6.3/lib:/risapps/rhel6/gcc/4.8.1/lib64:/usr/lib64"${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
#export LD_LIBRARY_PATH=/lib64:/risapps/rhel6/gcc/4.8.1/lib64:/usr/lib64:${LD_LIBRARY_PATH}
# export PATH=/scratch/genomic_med/apps/spseq/speedseq/bin:${PATH}
# module load root/5.34_25
############################## END EXPERIMENTAL ############################

## END ##
