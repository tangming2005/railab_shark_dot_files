## Set up HPC environment

For `bash` shell

Assuming `~/.bashrc` & `~/.bash_profile` is present, add following lines of code to `~/.bash_profile`. These lines in exact order as below should precede remaining contents of `~/.bash_profile`, if any.

```
# .bash_profile at HPC MDA
# v 1.2.1

# Source .bashrc first in environment
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

# The default umask is now handled by pam_umask.
# See pam_umask(8) and /etc/login.defs.

mypathmunge () {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ] ; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}

if [ -d $HOME/.profile.d ]; then
  for i in $HOME/.profile.d/*.sh; do
    if [ -r $i ]; then
        if [ "${-#*i}" != "$-" ]; then
            . "$i" >/dev/null 2>&1
        else
            . "$i" >/dev/null 2>&1
        fi
    fi
  done
  unset i
fi

unset -f mypathmunge

########### START USER SPECIFIFED CONTENTS ###########

# For configurations that may change env PATH, MODULES, LD_LIBRARY_PATH, PYTHONPATH, etc., prefer adding those settings into appropriate shell scripts under ~/.profile.d/*.sh. To add new executable to PATH, use mypathmunge function in any of shell scripts under ~/.profile.d/.

# bash aliases should be either in ~/.bashrc or ~/.bash_aliases.

# PS: any file ending .sh with executable file flag under ~/.profile.d/ directory will be executed at every login in HPC login and/or compute node.

## END ##
```

END

